CUDA = nvcc
CUDA_FLAGS = -arch=sm_20 -m64 -G -g

BIN = matrix-reduction

all: $(BIN)

matrix-reduction: main.cu
	$(CUDA) $(CUDA_FLAGS) -o $@ $<

clean:
	$(RM) $(BIN)
