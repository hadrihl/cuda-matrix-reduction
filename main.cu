#include <stdio.h>
#include <stdlib.h>

#define BLOCKS_PER_GRID 1
#define THREADS_PER_BLOCK 10
#define WIDTH 5

// matrix func
typedef struct {
	int height;
	int width;
	int *elements;
} Matrix;

// forward declaration
void traverseByCol(Matrix);
void traverseByRow(Matrix);

// kernel function
__global__ void kernel(int *d_data) {
	int i;
	__shared__ int s_data[10];
	unsigned idx = threadIdx.x + blockIdx.x * blockDim.x;

	// init shared memory to zero
	s_data[idx] = 0;

	// testing for thread 1 (idx)
	if(idx == 0) {
		printf("\n\nThreadID = %d\n\n", idx);
	for(i = 0; i < 5; i++) {
		printf("d_data[%d] = %d,\t", idx * 5 + i, d_data[idx * 5 + i]);
		s_data[idx] += d_data[idx * 5 + i];
		printf("s_data[%d] = %d\n", idx, s_data[idx]);
	}
	__syncthreads();
	}
}

// main **
int main() {
	int e, f;
	cudaError_t error;

	// matrix definition
	Matrix h_data;
	int *d_data;

	h_data.height = 10; h_data.width = 5;
	int size = h_data.height * h_data.width;
	h_data.elements = (int*) calloc(size, sizeof(int));

	// put dummy values in matrix/vector
	for(e = 0; e < h_data.height; e++) {
		for(f = 0; f < h_data.width; f++) {
			h_data.elements[e * h_data.width + f] = 1;//(e * h_data.width + f);
			printf("%d ", h_data.elements[e * h_data.width + f]);
		}
		printf("\n");
	}

	// device mem alloc
	size_t cudaSize = size * sizeof(int);
	error = cudaMalloc(&d_data, cudaSize);

	if(error != cudaSuccess) {
		fprintf(stderr, "cudaMalloc failed: %s\n", cudaGetErrorString(error));
		exit(1);
	}

	// copy host to device
	error = cudaMemcpy(d_data, h_data.elements, size, cudaMemcpyHostToDevice);

	if(error != cudaSuccess) {
		fprintf(stderr, "cudaMemcpyDeviceToHost failed: %s\n", cudaGetErrorString(error));
		exit(1);
	}

	// kernel launches
	kernel<<< BLOCKS_PER_GRID, THREADS_PER_BLOCK >>>(d_data);

	// copy device to host
	error = cudaMemcpy(h_data.elements, d_data, size, cudaMemcpyDeviceToHost);

	if(error != cudaSuccess) {
		fprintf(stderr, "cudaMemcpyDeviceToHost failed: %s\n", cudaGetErrorString(error));
		exit(1);
	}

	traverseByRow(h_data);
	traverseByCol(h_data);

	// clean up memory
	error = cudaFree(d_data);
	
	if(error != cudaSuccess) {
		fprintf(stderr, "cudaFree failed: %s\n", cudaGetErrorString(error));
		exit(1);
	}

	return 0;
}

void traverseByCol(Matrix data) {
	printf("\n\nTraverse by Column\n");
	int e, f;

	for(e = 0; e < data.width; e++) {
		for(f = 0; f < data.height; f++)
			printf("[%d]", e + WIDTH * f);
		printf("\n");
	}
}

void traverseByRow(Matrix data) {
	printf("\n\nTraverse by Row\n");
	int e, f;

	for(e = 0; e < data.height; e++) {
		for(f = 0; f < data.width; f++)
			printf("[%d]", e * WIDTH + f);
		printf("\n");
	}
}
